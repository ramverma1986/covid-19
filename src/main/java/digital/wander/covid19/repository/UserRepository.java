package digital.wander.covid19.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import digital.wander.covid19.model.User;

public interface UserRepository extends JpaRepository<User, String> {

}
