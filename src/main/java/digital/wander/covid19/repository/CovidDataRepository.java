package digital.wander.covid19.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import digital.wander.covid19.model.CovidData;

public interface CovidDataRepository extends JpaRepository<CovidData, String> {

}
