package digital.wander.covid19.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import digital.wander.covid19.model.Role;

public interface RoleRepository extends JpaRepository<Role, String> {

}
