package digital.wander.covid19.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
	private String exceptionHandler(Exception e) {

		log.error("Covid-19 Error: ", e);
		return "error";

	}

}
