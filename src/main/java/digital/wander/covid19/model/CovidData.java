package digital.wander.covid19.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class CovidData {

	@Id
	private String country;

	@JsonProperty("cases_confirmed")
	private int totalCase;

	@Getter(value = AccessLevel.NONE)
	private int activeCase;

	@JsonProperty("cases_recovered")
	private int recoveredCase;

	@JsonProperty("cases_death")
	private int deathCase;

	@OneToMany(cascade = CascadeType.ALL)
	@JsonProperty("states")
	private List<CaseDetail> caseDetails;

	public int getActiveCase() {
		return this.totalCase - this.recoveredCase - this.deathCase;
	}

}
