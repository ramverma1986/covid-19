package digital.wander.covid19.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Data
public class User {

	@Id
	private String username;

	private String password;

	@Transient
	private String passwordConfirm;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Role role;

}
