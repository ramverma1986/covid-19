package digital.wander.covid19.service;

import java.util.Arrays;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import digital.wander.covid19.model.Role;
import digital.wander.covid19.model.User;
import digital.wander.covid19.repository.RoleRepository;
import digital.wander.covid19.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@PostConstruct
	private void init() {

		Role adminRole = new Role();
		adminRole.setName("ADMIN");

		Role userRole = new Role();
		userRole.setName("USER");

		roleRepository.saveAll(Arrays.asList(adminRole, userRole));
	}

	public void save(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setRole(roleRepository.findById("USER").get());
		userRepository.save(user);
	}

	public User findByUsername(String username) {
		Optional<User> optional = userRepository.findById(username);
		return optional.get();
	}
}
