package digital.wander.covid19.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import digital.wander.covid19.model.CovidData;
import digital.wander.covid19.repository.CovidDataRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DashboardService {

	@Value("${covid.19.data.url}")
	private String covidDataUrl;

	@Value("${covid.default.country}")
	private String covidDefaultCountry;

	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private CovidDataRepository covidDataRepository;

	@PostConstruct
	private void init() {
		restTemplate = new RestTemplate();
	}

	public CovidData getCovidData(String country) {

		country = country == null ? this.covidDefaultCountry : country;

		Optional<CovidData> optional = covidDataRepository.findById(country);
		CovidData covidData = null;
		if (!optional.isPresent()) {
			covidData = this.fetchLatestCovidData(country);
			this.saveCovidData(country, covidData);
		} else {
			covidData = optional.get();
		}
		return covidData;
	}

	@SuppressWarnings("unchecked")
	private CovidData fetchLatestCovidData(String country) {

		Map<String, Object> allData = restTemplate.getForObject(covidDataUrl, Map.class);
		List<Map<String, Object>> countriesData = (List<Map<String, Object>>) allData.get("countries");

		for (Map<String, Object> countryData : countriesData) {

			if (country.equals(countryData.get("country"))) {
				return objectMapper.convertValue(countryData, CovidData.class);
			}

		}
		return null;
	}

	private void saveCovidData(String country, CovidData covidData) {

		covidData.setCountry(country);
		covidDataRepository.save(covidData);
	}

	@Scheduled(cron = "${covid.update.cron}")
	private void updateCovidData() throws InterruptedException {

		try {
			CovidData covidData = this.fetchLatestCovidData(this.covidDefaultCountry);
			this.saveCovidData(this.covidDefaultCountry, covidData);
		} catch (Exception e) {

			// In case of any exception, waiting for 15 minutes to re-execute the update
			log.error("Error while updating covid 19 data", e);
			Thread.sleep(900000);
			this.updateCovidData();
		}
	}
}
